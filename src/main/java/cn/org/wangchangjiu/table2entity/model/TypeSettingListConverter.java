package cn.org.wangchangjiu.table2entity.model;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.google.gson.Gson;
import com.intellij.util.xmlb.Converter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * @Classname ConfigSettingConverter
 * @Description
 * @Date 2023/5/15 20:34
 * @Created by wangchangjiu
 */
public class TypeSettingListConverter extends Converter<List<TypeSettingsStorage.TypeSetting>> {

    @Override
    public @Nullable List<TypeSettingsStorage.TypeSetting> fromString(@NotNull String value) {
        return JSONArray.parseArray(value, TypeSettingsStorage.TypeSetting.class);
    }

    @Override
    public @Nullable String toString(@NotNull List<TypeSettingsStorage.TypeSetting> value) {
        return JSONArray.toJSONString(value);
    }
}
