package cn.org.wangchangjiu.table2entity.model;

import com.google.gson.Gson;
import com.intellij.util.xmlb.Converter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @Classname ConfigSettingConverter
 * @Description
 * @Date 2023/5/15 20:34
 * @author by wangchangjiu
 */
public class ConfigSettingConverter extends Converter<CodePathConfig> {

    @Override
    public @Nullable CodePathConfig fromString(@NotNull String value) {
        Gson gson = new Gson();
        return gson.fromJson(value, CodePathConfig.class);
    }

    @Override
    public @Nullable String toString(@NotNull CodePathConfig value) {
        Gson gson = new Gson();
        return gson.toJson(value, CodePathConfig.class);
    }
}
