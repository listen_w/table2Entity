package cn.org.wangchangjiu.table2entity.model;

import java.io.Serializable;

/**
 * @Classname ConfigSetting
 * @Description
 * @Date 2023/5/11 16:40
 * @author wangchangjiu
 */
public class CodePathConfig implements Serializable {

    private String codeAuthor;

    private String entityPath;
    private String myBatisEntityPath;

    private String daoPath;

    private String mapperPath;

    private String mapperXmlPath;

    private boolean generateJpa = true;

    private boolean generateMyBatisPlus;




    public String getCodeAuthor() {
        return codeAuthor;
    }

    public void setCodeAuthor(String codeAuthor) {
        this.codeAuthor = codeAuthor;
    }

    public String getEntityPath() {
        return entityPath;
    }

    public void setEntityPath(String entityPath) {
        this.entityPath = entityPath;
    }

    public String getMyBatisEntityPath() {
        return myBatisEntityPath;
    }

    public void setMyBatisEntityPath(String myBatisEntityPath) {
        this.myBatisEntityPath = myBatisEntityPath;
    }

    public String getDaoPath() {
        return daoPath;
    }

    public void setDaoPath(String daoPath) {
        this.daoPath = daoPath;
    }

    public String getMapperPath() {
        return mapperPath;
    }

    public void setMapperPath(String mapperPath) {
        this.mapperPath = mapperPath;
    }

    public String getMapperXmlPath() {
        return mapperXmlPath;
    }

    public void setMapperXmlPath(String mapperXmlPath) {
        this.mapperXmlPath = mapperXmlPath;
    }

    public boolean isGenerateJpa() {
        return generateJpa;
    }

    public void setGenerateJpa(boolean generateJpa) {
        this.generateJpa = generateJpa;
    }

    public boolean isGenerateMyBatisPlus() {
        return generateMyBatisPlus;
    }

    public void setGenerateMyBatisPlus(boolean generateMyBatisPlus) {
        this.generateMyBatisPlus = generateMyBatisPlus;
    }
}
