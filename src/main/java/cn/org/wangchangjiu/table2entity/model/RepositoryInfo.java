package cn.org.wangchangjiu.table2entity.model;

import java.io.Serializable;
import java.util.Set;

/**
 * @Classname RepositoryInfo
 * @Description
 * @Date 2023/9/11 20:15
 * @author wangchangjiu
 */
public class RepositoryInfo implements Serializable {

    private String codeAuthor;

    private String packagePath;

    /**
     *  导入的包
     */
    private Set<String> importPackage;

    /**
     *  Repository 描述
     */
    private String desc;

    /**
     *  repository 名称
     */
    private String repositoryName;

    /**
     *  实体名称
     */
    private String entityName;

    /**
     *  主键类型
     */
    private String idType;

    public String getCodeAuthor() {
        return codeAuthor;
    }

    public void setCodeAuthor(String codeAuthor) {
        this.codeAuthor = codeAuthor;
    }

    public String getPackagePath() {
        return packagePath;
    }

    public void setPackagePath(String packagePath) {
        this.packagePath = packagePath;
    }

    public Set<String> getImportPackage() {
        return importPackage;
    }

    public void setImportPackage(Set<String> importPackage) {
        this.importPackage = importPackage;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }
}
