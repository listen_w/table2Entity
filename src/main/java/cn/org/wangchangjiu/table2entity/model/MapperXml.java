package cn.org.wangchangjiu.table2entity.model;

import java.io.Serializable;
import java.util.List;

/**
 * @Classname MapperXml
 * @Description
 * @Date 2023/9/18 19:45
 * @author  wangchangjiu
 */
public class MapperXml implements Serializable {

    private String namespace;

    private String entityName;

    private List<MapperXmlColumn> ids;

    private List<MapperXmlColumn> results;

    private String columnNames;

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public List<MapperXmlColumn> getIds() {
        return ids;
    }

    public void setIds(List<MapperXmlColumn> ids) {
        this.ids = ids;
    }

    public List<MapperXmlColumn> getResults() {
        return results;
    }

    public void setResults(List<MapperXmlColumn> results) {
        this.results = results;
    }

    public String getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(String columnNames) {
        this.columnNames = columnNames;
    }

    public static class MapperXmlColumn {

        private String property;

        private String columnName;

        private String jdbcType;

        public String getProperty() {
            return property;
        }

        public void setProperty(String property) {
            this.property = property;
        }

        public String getColumnName() {
            return columnName;
        }

        public void setColumnName(String columnName) {
            this.columnName = columnName;
        }

        public String getJdbcType() {
            return jdbcType;
        }

        public void setJdbcType(String jdbcType) {
            this.jdbcType = jdbcType;
        }
    }

}
