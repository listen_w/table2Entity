package cn.org.wangchangjiu.table2entity.model;

import java.io.Serializable;
import java.util.List;

/**
 * @Classname ColumnInfo
 * @Description
 * @Date 2023/5/8 11:11
 * @author  wangchangjiu
 */
public class ColumnInfo implements Serializable {

    /**
     *  注解
     */
    private List<String> annotations;

    /**
     *  列描述
     */
    private String desc;

    /**
     *  列类型
     */
    private String type;

    /**
     *  jdbc 列类型
     */
    private String jdbcType;

    /**
     *  列名字
     */
    private String name;

    /**
     *  表中列名称
     */
    private String jdbcColumnName;

    /**
     *  列类型导入的包
     */
    private String typeImportPck;

    /**
     *  该列是否是主键
     */
    private boolean primaryKey = false;

    public List<String> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<String> annotations) {
        this.annotations = annotations;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getJdbcType() {
        return jdbcType;
    }

    public void setJdbcType(String jdbcType) {
        this.jdbcType = jdbcType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getJdbcColumnName() {
        return jdbcColumnName;
    }

    public void setJdbcColumnName(String jdbcColumnName) {
        this.jdbcColumnName = jdbcColumnName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeImportPck() {
        return typeImportPck;
    }

    public void setTypeImportPck(String typeImportPck) {
        this.typeImportPck = typeImportPck;
    }

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }
}
