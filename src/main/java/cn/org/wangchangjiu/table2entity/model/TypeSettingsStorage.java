package cn.org.wangchangjiu.table2entity.model;

import com.alibaba.fastjson.JSONArray;
import com.intellij.ide.fileTemplates.impl.UrlUtil;
import com.intellij.util.ExceptionUtil;

import java.util.List;

/**
 * @Classname TypeSettingsStorage
 * @Description
 * @Date 2023/8/29 21:37
 * @author by wangchangjiu
 */
public class TypeSettingsStorage {


    public static List<TypeSetting> defaultVal() {
        try {
            // 从配置文件中加载配置
            String json = UrlUtil.loadText(TypeSettingsStorage.class.getResource("/typeSetting/default.json"));
            return JSONArray.parseArray(json, TypeSetting.class);
        } catch (Exception e) {
            ExceptionUtil.rethrow(e);
        }
        return null;
    }


    public static class TypeSetting {

        private String columnType;

        private String javaType;

        private String jdbcType;

        private String matchType;

        private String importPck;

        public TypeSetting(String columnType, String javaType, String importPck, String jdbcType, String matchType){
            this.columnType = columnType;
            this.javaType = javaType;
            this.jdbcType = jdbcType;
            this.importPck = importPck;
            this.matchType = matchType;
        }

        public String getColumnType() {
            return columnType;
        }

        public void setColumnType(String columnType) {
            this.columnType = columnType;
        }

        public String getJavaType() {
            return javaType;
        }

        public String getJdbcType() {
            return jdbcType;
        }

        public void setJdbcType(String jdbcType) {
            this.jdbcType = jdbcType;
        }

        public void setJavaType(String javaType) {
            this.javaType = javaType;
        }

        public String getMatchType() {
            return matchType;
        }

        public void setMatchType(String matchType) {
            this.matchType = matchType;
        }

        public String getImportPck() {
            return importPck;
        }

        public void setImportPck(String importPck) {
            this.importPck = importPck;
        }

        public static TypeSettingsStorage.TypeSetting objectType(){
            return new TypeSetting("", "Object", "","java.lang.Object", "");
        }
    }
}
