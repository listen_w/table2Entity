package cn.org.wangchangjiu.table2entity.extension;

import cn.org.wangchangjiu.table2entity.model.TypeSettingListConverter;
import cn.org.wangchangjiu.table2entity.model.TypeSettingsStorage;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.annotations.OptionTag;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * @Classname TypeSettingCache
 * @Description
 * @Date 2023/8/29 21:15
 * @author by wangchangjiu
 */
@State(name = "typeSetting.cache", storages = {@Storage(value = "typeSetting-cache.xml")})
public class TypeSettingCache implements PersistentStateComponent<TypeSettingCache> {

    @OptionTag(converter = TypeSettingListConverter.class)
    private List<TypeSettingsStorage.TypeSetting> settings;

    public static TypeSettingCache getInstance() {
        return ApplicationManager.getApplication().getService(TypeSettingCache.class);
    }

    public List<TypeSettingsStorage.TypeSetting> getSettings(){
        if(this.settings == null || this.settings.size() == 0){
            return TypeSettingsStorage.defaultVal();
        }
        return this.settings;
    }

    public void setSettings(List<TypeSettingsStorage.TypeSetting> settings) {
        this.settings = settings;
    }

    @Override
    public @Nullable TypeSettingCache getState() {
        return this;
    }

    @Override
    public void loadState(@NotNull TypeSettingCache state) {
        if (state.settings == null) {
            return;
        }
        this.settings = state.settings;
    }
}
