package cn.org.wangchangjiu.table2entity.extension;

import cn.org.wangchangjiu.table2entity.model.TypeSettingsStorage;
import cn.org.wangchangjiu.table2entity.ui.TypeSettingUI;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.util.NlsContexts;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Vector;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Classname TypeSettingConfigurable
 * @Description
 * @Date 2023/8/29 20:27
 * @author by wangchangjiu
 */
public class TypeSettingConfigurable implements Configurable {

    private TypeSettingUI typeSettingUI;

    public TypeSettingConfigurable(){
        this.typeSettingUI = new TypeSettingUI();

        TypeSettingCache state = TypeSettingCache.getInstance();
        // 第一次打开配置页面，没有持久化数据，这个state为null
        List<TypeSettingsStorage.TypeSetting> settings;
        List<TypeSettingsStorage.TypeSetting> defaultVal = TypeSettingsStorage.defaultVal();
        if(state != null){
            settings = Optional.ofNullable(state.getSettings()).orElse(defaultVal);
        } else {
            settings = defaultVal;
        }
        JTable table = this.typeSettingUI.getTypeSettingTable();
        DefaultTableModel tableModel = (DefaultTableModel)table.getModel();

        Map<String, TypeSettingsStorage.TypeSetting> settingMap = defaultVal.stream().collect(Collectors.toMap(TypeSettingsStorage.TypeSetting::getColumnType, Function.identity()));
        settings.stream().forEach(setting -> {
            if(StringUtils.isEmpty(setting.getJdbcType()) && settingMap.containsKey(setting.getColumnType())){
                TypeSettingsStorage.TypeSetting typeSetting = settingMap.get(setting.getColumnType());
                setting.setJdbcType(typeSetting.getJdbcType());
            }
            Object[] row = new Object[]{setting.getColumnType(), setting.getJavaType(), setting.getImportPck(), setting.getJdbcType(), setting.getMatchType()};
            tableModel.addRow(row);
        });
        tableModel.fireTableDataChanged();
        table.repaint();
    }

    @Override
    public @NlsContexts.ConfigurableName String getDisplayName() {
        return "typeSetting";
    }

    @Override
    public @Nullable JComponent createComponent() {
        return this.typeSettingUI.getMainPanel();
    }

    @Override
    public boolean isModified() {
        return true;
    }

    @Override
    public void apply() throws ConfigurationException {
        JTable table = this.typeSettingUI.getTypeSettingTable();
        if(table.getCellEditor() != null){
            table.getCellEditor().stopCellEditing();
        }

        DefaultTableModel tableModel = (DefaultTableModel)table.getModel();
        Vector<Vector> dataVector = tableModel.getDataVector();
        List<TypeSettingsStorage.TypeSetting> typeSettings = dataVector.stream().map(vector -> new TypeSettingsStorage.TypeSetting(vector.get(0).toString(), vector.get(1).toString(),
                vector.get(2).toString(), vector.get(3).toString(), vector.get(4).toString())).collect(Collectors.toList());
       TypeSettingCache.getInstance().setSettings(typeSettings);
    }
}
