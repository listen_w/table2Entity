package cn.org.wangchangjiu.table2entity.extension;

import cn.org.wangchangjiu.table2entity.model.CodePathConfig;
import cn.org.wangchangjiu.table2entity.model.ConfigSettingConverter;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.project.Project;
import com.intellij.util.xmlb.annotations.OptionTag;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@State(name = "configSetting.cache", storages = {@Storage(value = "configSetting-cache.xml")})
public class PathConfigSettingCache implements PersistentStateComponent<PathConfigSettingCache> {

    @OptionTag(converter = ConfigSettingConverter.class)
    private CodePathConfig codePathConfig;

    public static PathConfigSettingCache getInstance(Project project) {
        if(project == null){
            return null;
        }
        return project.getService(PathConfigSettingCache.class);
    }

    public void addConfig(CodePathConfig codePathConfig){
        this.codePathConfig = codePathConfig;
    }

    public CodePathConfig getConfig(){
       return this.codePathConfig;
    }


    /**
     * getState() 方法在每次修改数据被保存时都会调用，
     * 该方法返回配置对象，以 XML 协议序列化后存储到文件中
     * @return
     */
    @Override
    public @Nullable PathConfigSettingCache getState() {
        return this;
    }

    /**
     *  IDE 启动时，会加载已安装的插件，实现的 loadState() 方法，在插件被加载时会被调用，IDE 将数据反序列化为对象，作为方法的入参数
     * @param state loaded component state
     */
    @Override
    public void loadState(@NotNull PathConfigSettingCache state) {
        if (state.codePathConfig == null) {
            return;
        }
        this.codePathConfig = state.codePathConfig;
    }
}
