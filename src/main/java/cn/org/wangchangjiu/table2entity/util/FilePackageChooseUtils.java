package cn.org.wangchangjiu.table2entity.util;

import com.intellij.ide.util.PackageChooserDialog;
import com.intellij.openapi.fileChooser.FileChooser;
import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiPackage;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;

/**
 * @Classname CommonUtil
 * @Description
 * @Date 2023/5/8 14:15
 * @author wangchangjiu
 */
public class FilePackageChooseUtils {

    /**
     * 选择文件并设置路径
     *
     * @param project 项目
     * @param pathTf  选择并设置JTextField路径
     */
    public static void chooseFileAndSetPath(Project project, JTextField pathTf) {
        VirtualFile path = project.getBaseDir();
        String oldPath = pathTf.getText();
        if(StringUtils.isNotBlank(oldPath) && !oldPath.contains(path.getPath())){
            oldPath = path.getPath() + "/" + oldPath;
        }
        if (FileUtil.exists(oldPath)) {
            VirtualFile virtualFile = LocalFileSystem.getInstance().findFileByPath(oldPath);
            if (virtualFile != null) {
                path = virtualFile;
            }
        }

        VirtualFile virtualFile = FileChooser.chooseFile(FileChooserDescriptorFactory.createSingleFolderDescriptor(), project, path);
        if (virtualFile != null) {
            pathTf.setText(virtualFile.getPath().replace(project.getBasePath() + "/", ""));
        }
    }



    /**
     * 选择包并设置包路径
     *
     * @param project       项目
     * @param packagePathTf 包路径输入框
     */
    public static void choosePackageAndSetPackagePath(Project project, JTextField packagePathTf) {
        PackageChooserDialog selector = new PackageChooserDialog("Select a Package", project);
        selector.show();
        PsiPackage selectedPackage = selector.getSelectedPackage();
        String path = "src/main/java";
        if (selectedPackage != null) {
            if (selectedPackage.getQualifiedName() != null && selectedPackage.getQualifiedName().length() > 0) {
                path += "/" + selectedPackage.getQualifiedName().replace(".", "/");
            }
        }
        packagePathTf.setText(path);
    }


}
