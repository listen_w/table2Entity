package cn.org.wangchangjiu.table2entity.util;


public enum MatchType {
    /**
     * 正常的
     */
    ORDINARY,
    /**
     * 正则表达式
     */
    REGEX,
}
