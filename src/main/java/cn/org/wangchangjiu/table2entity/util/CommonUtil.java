package cn.org.wangchangjiu.table2entity.util;

import org.apache.commons.lang3.StringUtils;

import java.sql.JDBCType;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Classname CommonUtil
 * @Description
 * @Date 2023/5/8 14:15
 * @author wangchangjiu
 */
public class CommonUtil {

    private static Pattern UNDERLINE_PATTERN = Pattern.compile("_([a-z])");

    private static Pattern QUOTATION_MARK_PATTERN = Pattern.compile("\\`(.*?)\\`");

    public static final String SEPARATOR = "|";


    public static String mybatisJdbcType(String sqlType) {
        if(StringUtils.isEmpty(sqlType)){
            return sqlType;
        }
        String name = JdbcType.forName(sqlType);
        return StringUtils.isEmpty(name) ? sqlType.toUpperCase() : name;
    }

    public static String dropQuotationMark(String name){
        if(name == null || name.length() == 0){
            return name;
        }
        Matcher matcher = QUOTATION_MARK_PATTERN.matcher(name);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return name;
    }

    public static String underlineToHump (String str, boolean titleCase){
        if(str == null || str.length() == 0){
            return str;
        }
        str = str.toLowerCase();
        Matcher matcher = UNDERLINE_PATTERN.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(0).toUpperCase());
        }
        matcher.appendTail(sb);
        if(titleCase){
            sb.setCharAt(0, (char) ((int)sb.charAt(0) - 32));
        }
        return sb.toString().replaceAll("_", "");
    }

    public static Set<String> initJpaEntityPackage(){
        Set<String> importPackages = new HashSet<>();
        importPackages.add("import lombok.Data;");
        importPackages.add("import javax.persistence.*;");
        return importPackages;
    }

    public static Set<String> initMybatisPlusEntityPackage(){
        Set<String> importPackages = new HashSet<>();
        importPackages.add("import lombok.Data;");
        importPackages.add("import com.baomidou.mybatisplus.annotation.*;");
        return importPackages;
    }


    public static String pathToPackage(String fullPath){
        if (!StringUtils.isEmpty(fullPath)) {
            return fullPath.replace("/", ".");
        }
        return fullPath;
    }

    public static String pathToImportPackage(String fullPath){
        String fullPackage = "";
        if (!StringUtils.isEmpty(fullPath)) {
            fullPackage = fullPath.replace("/", ".");
        }
        fullPackage = fullPackage.replace("src.main.java.", "import ");
        return fullPackage + ";";
    }

    public static String pathToDeclarePackage(String fullPath){
        String fullPackage = "";
        if (!StringUtils.isEmpty(fullPath)) {
            fullPackage = fullPath.replace("/", ".");
        }
        fullPackage = fullPackage.replace("src.main.java.", "");
        return fullPackage;
    }

    /**
     * 根据全类名获取类名
     *
     * @param fullClassName 全类名
     * @return 类名
     */
    public static String getClassNameByFullClassName(String fullClassName) {
        String className = StringUtils.substringAfterLast(fullClassName, ".");
        return StringUtils.isNotBlank(className) ? className : fullClassName;
    }

    /**
     * 根据全类名获取包名
     *
     * @param fullClassName 全类名
     * @return 包名
     */
    public static String getPackageNameByFullClassName(String fullClassName) {
        return StringUtils.substringBeforeLast(fullClassName, ".");
    }

    public static void main(String[] args) {
       System.out.println( underlineToHump("too", false));
    }

}
