package cn.org.wangchangjiu.table2entity.service;

import cn.org.wangchangjiu.table2entity.model.ColumnInfo;
import cn.org.wangchangjiu.table2entity.model.MapperXml;
import cn.org.wangchangjiu.table2entity.model.RepositoryInfo;
import cn.org.wangchangjiu.table2entity.model.TableInfo;
import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * @Classname JpaRepositoryParser
 * @Description
 * @Date 2023/9/11 20:29
 * @author wangchangjiu
 */
public class MyBatisPlusParser {

    public static RepositoryInfo parser(TableInfo result){
        RepositoryInfo repositoryInfo = new RepositoryInfo();

        Set<String> importPackage = new HashSet<>();
        importPackage.add(result.getClassPackage());

        repositoryInfo.setRepositoryName(result.getEntityName() + "Mapper");
        repositoryInfo.setEntityName(result.getEntityName());

        repositoryInfo.setDesc(StringUtils.isEmpty(result.getDesc()) ? repositoryInfo.getRepositoryName() : result.getDesc() + " Mapper");
        repositoryInfo.setImportPackage(importPackage);
        return repositoryInfo;
    }




}
