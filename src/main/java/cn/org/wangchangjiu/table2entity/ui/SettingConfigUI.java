package cn.org.wangchangjiu.table2entity.ui;

import cn.org.wangchangjiu.table2entity.model.CodePathConfig;
import cn.org.wangchangjiu.table2entity.util.FilePackageChooseUtils;
import com.intellij.ide.impl.ProjectUtil;
import com.intellij.ide.util.PackageChooserDialog;
import com.intellij.psi.PsiPackage;

import javax.swing.*;

/**
 * @Classname SettingConfigUI
 * @Description
 * @Date 2023/5/11 15:22
 * @Created by wangchangjiu
 */
public class SettingConfigUI {

    private JLabel packageLabel;
    private JTextField entityTextField;

    private JButton entityButton;
    private JLabel pathLabel;
    private JTextField daoTextField;

    private JPanel settingPanel;
    private JTextField codeAuthorField;
    private JButton daoButton;
    private JTextField mapperTextField;

    private JButton mapperButton;
    private JTextField mapperXmlTextField;

    private JButton mapperXmlButton;
    private JTextField myBaitsEntityText;
    private JButton myBatisEntityBut;
    private JCheckBox jpaCheckBox;
    private JCheckBox myBatisPlusCheckBox;

    private CodePathConfig codePathConfig = new CodePathConfig();

    public SettingConfigUI(){

        jpaCheckBox.setSelected(true);
        entityButton.addActionListener(actionEvent -> FilePackageChooseUtils.choosePackageAndSetPackagePath(ProjectUtil.getActiveProject(), entityTextField));
        daoButton.addActionListener(actionEvent -> FilePackageChooseUtils.choosePackageAndSetPackagePath(ProjectUtil.getActiveProject(), daoTextField));
        mapperButton.addActionListener(actionEvent -> FilePackageChooseUtils.choosePackageAndSetPackagePath(ProjectUtil.getActiveProject(), mapperTextField));
        mapperXmlButton.addActionListener(actionEvent -> FilePackageChooseUtils.chooseFileAndSetPath(ProjectUtil.getActiveProject(), mapperXmlTextField));
        myBatisEntityBut.addActionListener(actionEvent -> FilePackageChooseUtils.choosePackageAndSetPackagePath(ProjectUtil.getActiveProject(), myBaitsEntityText));

    }


    public void fillPath(CodePathConfig codePathConfig){
        this.entityTextField.setText(codePathConfig.getEntityPath());
        this.daoTextField.setText(codePathConfig.getDaoPath());
        this.mapperTextField.setText(codePathConfig.getMapperPath());
        this.mapperXmlTextField.setText(codePathConfig.getMapperXmlPath());
        this.codeAuthorField.setText(codePathConfig.getCodeAuthor());
        this.myBaitsEntityText.setText(codePathConfig.getMyBatisEntityPath());
        this.jpaCheckBox.setSelected(codePathConfig.isGenerateJpa());
        this.myBatisPlusCheckBox.setSelected(codePathConfig.isGenerateMyBatisPlus());
        this.codePathConfig = codePathConfig;
    }

    public CodePathConfig getCodePathConfig() {
        this.codePathConfig.setCodeAuthor(this.codeAuthorField.getText());
        this.codePathConfig.setDaoPath(this.daoTextField.getText());
        this.codePathConfig.setMyBatisEntityPath(this.myBaitsEntityText.getText());
        this.codePathConfig.setEntityPath(this.entityTextField.getText());
        this.codePathConfig.setMapperPath(this.mapperTextField.getText());
        this.codePathConfig.setMapperXmlPath(this.mapperXmlTextField.getText());
        this.codePathConfig.setGenerateJpa(this.jpaCheckBox.isSelected());
        this.codePathConfig.setGenerateMyBatisPlus(this.myBatisPlusCheckBox.isSelected());
        return codePathConfig;
    }

    public JPanel getSettingPanel() {
        return settingPanel;
    }

}
