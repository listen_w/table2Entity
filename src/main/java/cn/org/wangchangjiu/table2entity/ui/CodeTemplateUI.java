package cn.org.wangchangjiu.table2entity.ui;

import javax.swing.*;

/**
 * @Classname CodeTemplateUI
 * @Description
 * @Date 2023/9/12 20:17
 * @Created by wangchangjiu
 */
public class CodeTemplateUI {
    private JTextArea jpaEntityArea;
    private JTextArea JpaRepositoryArea;
    private JTextArea mybatisEntityArea;
    private JTextArea mapperArea;
    private JTextArea mapperXmlArea;
    private JPanel mainPanel;
    private JScrollPane jpaEntityPane;

    public CodeTemplateUI(){

        this.mainPanel = new JPanel();

        this.jpaEntityArea = new JTextArea();
        this.jpaEntityPane = new JScrollPane(this.jpaEntityArea);
        this.mainPanel.add(this.jpaEntityPane);

        this.JpaRepositoryArea = new JTextArea();
        this.mainPanel.add(this.JpaRepositoryArea);

        this.mybatisEntityArea = new JTextArea();
        this.mainPanel.add(this.mybatisEntityArea);

        this.mapperArea = new JTextArea();
        this.mainPanel.add(this.mapperArea);

        this.mapperXmlArea = new JTextArea();
        this.mainPanel.add(this.mapperXmlArea);

        this.mainPanel.updateUI();

    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

}
