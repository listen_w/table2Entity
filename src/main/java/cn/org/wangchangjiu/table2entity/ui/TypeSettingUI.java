package cn.org.wangchangjiu.table2entity.ui;

import com.intellij.ui.components.JBScrollPane;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * @Classname typeSetting
 * @Description
 * @Date 2023/8/29 20:39
 * @Created by wangchangjiu
 */
public class TypeSettingUI {
    private JButton add;
    private JButton reduce;
    private JTable typeSettingTable;
    private JScrollPane typeSettingScrollPane;
    private JPanel mainPanel;

    public TypeSettingUI(){

        String[] header = {"列类型", "java类型", "java包", "jdbc类型", "匹配方式(regex|ordinary)"};
        DefaultTableModel tableModel = new DefaultTableModel(null, header);
        this.typeSettingTable = new JTable();
        this.typeSettingTable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        this.typeSettingTable.setModel(tableModel);
        this.typeSettingScrollPane = new JBScrollPane(typeSettingTable);
        this.typeSettingScrollPane.setViewportView(typeSettingTable);
        this.typeSettingScrollPane.setSize(200, 800);
        this.mainPanel.add(this.typeSettingScrollPane);

        this.add.addActionListener(actionEvent -> tableModel.addRow(new Object[]{ "", "", "", "", ""}));
        this.reduce.addActionListener(actionEvent -> {
            if(tableModel.getRowCount() - 1 >= 0) {
                tableModel.removeRow(tableModel.getRowCount() - 1);
            }
        });
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JTable getTypeSettingTable() {
        return typeSettingTable;
    }
}
